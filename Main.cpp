#include "MyArray.h"
#include "RandMT.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <cstring>

using namespace std;



int main(int argc, char** argv) {
	MyArray number;
	MyArray helper;
	string name;
	int size;
	int choise;
	int aem;
	double bubbleTime;
	double selectTIme;
	double insertTime;
	double quickTime;
	clock_t timeStart;
	clock_t timeFinised;
	ofstream fout;
	
	fout.open("Times.txt"); 
	cout<< "Student" <<endl << "===================================" <<endl;
	cout << "Name: ";
    getline(cin, name);
	fflush(stdin);
	cout << "AEM: ";
	cin >> aem;
	cout<< "===================================" <<endl << endl;
	cout<< "Computer characteristics" <<endl << "==================================="<<endl;
	cout << "CPU: Inter(R) Core(TM) i5-4570 CPU @ 3.20GHz" <<endl;
	cout << "Ram size: 8GB "<<endl;
	cout << "===================================" <<endl << endl;
	cout << "Give Array a Size: ";
	cin >> size;
	number.setArraySize(size);
	number.createArray();
	helper.setArraySize(size);
	helper.createArray();
	cout << "How to initialize the table: " <<endl;
	cout << "1) 100% random" << endl;
	cout << "2) 50-50 random/sorted" << endl;
	cout << "3) Ascending" << endl;
	cout << "4) Descending" << endl;
	cout << "5) Exit" << endl;
	cout << "Option: ";
	cin >> choise;
	if(choise<5 && choise>0)
	{
	switch(choise) {
      case (1) :
         number.randomArray();
         break;
      case (2) :
      	number.fiftyArray();
         break;
      case (3) :
         number.ascArray();
         break;
      case (4) :
         number.descArray();
         break;
      default :
         cout << "Table not filled" << endl;
   			 }
	number.copyArray(helper);
	cout <<  "Sorting==>  ";
	
	timeStart=clock();
	helper.bubbleSort();
	timeFinised=clock();
	bubbleTime=(double)(timeFinised-timeStart)/CLOCKS_PER_SEC;
	cout<< "BubbleSort... ";
	fout << "BubbleSort : "<< bubbleTime <<"sec" << endl;

	number.copyArray(helper);
	timeStart=clock();
	helper.selectSort();
	timeFinised=clock();
	selectTIme=(double)(timeFinised-timeStart)/CLOCKS_PER_SEC;
	cout << "SelectSort... ";
	fout << "SelectSort : "<< selectTIme <<"sec" << endl;
	
	number.copyArray(helper);
	timeStart=clock();
	helper.insertSort();
	timeFinised=clock();
	insertTime=(double)(timeFinised-timeStart)/CLOCKS_PER_SEC;
	cout << "InsertSort... ";
	fout << "InsertSort : "<< insertTime <<"sec" << endl;

	number.copyArray(helper);
	timeStart=clock();
	helper.quickSort(0, size-1);
	timeFinised=clock();
	quickTime=(double)(timeFinised-timeStart)/CLOCKS_PER_SEC;
	cout << "QuickSort... ";
	fout << "QuickSort  : "<< quickTime <<"sec" << endl;
	
	
	fout.close();
	number.freeArrayMem();
	helper.freeArrayMem();
	};
	return 0;
}
