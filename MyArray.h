#ifndef MYARRAY_H
#define MYARRAY_H

class MyArray
{
	private:
		unsigned *customArray; //pinakas akeraiwn
		int size;
	public:
		void setArraySize(int n1);
		void createArray();
		void printArray();
		void freeArrayMem();
		void randomArray();
		void fiftyArray();
		void ascArray();
		void descArray();
		void bubbleSort();
		void selectSort();
		void copyArray(MyArray ant);
		void insertSort();
		void quickSort(int left, int right);
};



#endif
