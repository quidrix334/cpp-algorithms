#include "MyArray.h"
#include "RandMT.h"
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <fstream>


using namespace std; 

void MyArray::setArraySize(int givenSize)
{
		size=givenSize;
};

void MyArray::createArray()
{
		customArray = new unsigned[size];
};	
void MyArray::printArray()
{
	for(int i=0;i<size;i++)
	cout << customArray[i] << " "	;
	cout<<endl;
};

void MyArray::freeArrayMem()
{
	delete[]customArray;
};

void MyArray::randomArray()
{
	RandMT myRandom; 
	RandMT(time(NULL));
	for(int i=0;i<size;i++)
	{
	customArray[i] = myRandom();
	}
};

void MyArray::fiftyArray()
{
	int i; 
	RandMT myRandom;
	for(i=size/2;i<size;i++)
	{	RandMT(time(NULL));
		customArray[i] = myRandom();
	}
	for(i=0;i<size/2;i++)
	{
		customArray[i]= i+1;
	}
	
};


void MyArray::ascArray()
{
	int i;
	for(i=0;i<size;i++)
	customArray[i] = i*2+9;
};

void MyArray::descArray()
{
	for(int i=0;i<size;i++)
	customArray[i] = 100-i*2+9;
	
};

void MyArray::bubbleSort()
{
	int i,j,temp;
	for(i=1; i<size; i++)       
		for(j=size-1; j>=i; j--)             
			if(customArray[j-1] > customArray[j])               
				{                    
				temp = customArray[j-1];                    
				customArray[j-1] = customArray[j];                    
				customArray[j] = temp;               
				}  
}; 

void MyArray::selectSort()
{
	int i,j,k,min;
	for(i=0; i<size-1; i++)       
	{         
	k = i;         
	min = customArray[i];         
	for(j=i+1; j<size; j++)            
	{              
	if(customArray[j] < min)                
	{                  
	k = j;                  
	min = customArray[j];                
	}            
	}         
	customArray[k] = customArray[i];         
	customArray[i] = min;       
	} 
};

void MyArray::insertSort()
{
	int i,j,x;
	for(i=1; i<size; i++)
 	{
 	x = customArray[i];
 	j = i-1;
 	while( j>=0 && customArray[j]>x)
 		{
 		customArray[j+1] = customArray[j];
 		j = j-1;
 		}
 	customArray[j+1] = x;
 	}	 
};

void MyArray::quickSort(int left, int right)
{
 int i, j, mid, x, temp;
 if (left < right)
 {
 i = left;
 j = right;
 mid = (left+right)/2;
 x = customArray[mid];
 while (i < j)
 {
 while (customArray[i] < x)
 i++;
 while (customArray[j] > x)
 j--;
 if(i < j)
 {
 if(customArray[i] == customArray[j])
{
 if(i<mid)
 i++;
 if(j>mid)
 j--;
}
 else
 {
 temp = customArray[i];
 customArray[i] = customArray[j];
 customArray[j] = temp;
 }
 }
 }
 quickSort(left,j-1);
 quickSort(j+1,right);
 }
}



void MyArray::copyArray(MyArray ant)
{
	int i;
	for(i=0;i<size;i++)
		ant.customArray[i]=customArray[i];
}; 
